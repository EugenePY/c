#include <caml-kata.h>
#include <iostream>
#include <vector>
#include <type_traits>

int my_func(int a){
  return a;
}


void fk_main() {
  int a = 1;
  int *ptr;
  void *func;
  ptr = &a;
  //(int*) func = &a; // this is invalid in C++, https://eli.thegreenplace.net/2009/11/16/void-and-casts-in-c-and-c
  std::cout << func << std::endl;
  std::cout << &a << std::endl;
  std::cout << ptr << std::endl;
  std::cout << *ptr << std::endl;
  std::cout << &ptr << std::endl;
};


long queueTime(std::vector<int> customers,int n){
  int avaliable_tills = n;
  long time_for_checkout; 
  long time = 0;
  
  while (!customers.empty()){
    time_for_checkout = customers.back();
    customers.pop_back();

    if ( avaliable_tills > 0){
      time = std::max(time_for_checkout, time);
      avaliable_tills -= 1;
    } else {
      time = time + time_for_checkout;
      avaliable_tills = n - 1;
    }
  }
  return time;
}

void QueueTime::check(){
      std::cout << "check from abstract class" << std::endl;
  };

void QueueTime::setup(){
    std::cout << "setup from abstract class" << std::endl;
};

