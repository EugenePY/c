#include <iostream>

template <typename T>
void log_address_value(T var) {
  std::cout << "Address of variable " << &var << std::endl;
  std::cout << "value of variable " << var << std::endl;
  std::cout << "dereference of variable " << *var << std::endl;

}

int sum(int a, int b) {
  return (a + b);

};

class Object {

 public:

  int *resource_ptr;
  Object(int a) {
    resource_ptr = new int(a);
    std::cout << this << "object created res=" << resource_ptr << std::endl;
  };

  ~Object(){
    if (resource_ptr != NULL) {
      std::cout << this << "object delete" << std::endl;
      delete resource_ptr;
    }
  };
  // the argument must pass a reference of Object
  int operator+ (Object &b) {
    std::cout << this << " adding " << &b << std::endl;
    return *b.resource_ptr + *resource_ptr;
  };
  // ISSIUE: https://stackoverflow.com/questions/62353625/c-candidate-function-not-viabl://stackoverflow.com/questions/62353625/c-candidate-function-not-viable
  int operator+ (Object b) const {
    return *b.resource_ptr + *resource_ptr;
  }
};

void pointer_main(){
  int result;
  int *result_ptr;
  result = sum(1, 1);
  log_address_value(result_ptr);
  // even if there are value in *result_ptr, but the address didn't initialize
  // so the address didn't exist.
  result_ptr = &result;
  log_address_value(result_ptr);
  // or we can initial the point by the new keyword
  int *result_ptr_with_init = new int;
  *result_ptr_with_init = 1;
}

int func(int *a, int *b) {
  // passing by pointer
  // this mean the memory of a will be free immidiately
  return *a + *b;
}

int pass_by_value_func(int a, int b) {
  // passing by value.
  // this mean the memory of a will be free immidiately
  return a + b;
}

int func(int &a, int &b) {
  // passing by reference
  // the memory of a will not be free.
  return a + b;
}


bool is_even(int a){
  return (a % 2 == 0);
};
bool is_even(float f);

bool is_even(char str){
  return str;
};


void memory_management_main(){
  Object my_obj(1);
  Object my_obj2(2);
  log_address_value(my_obj.resource_ptr);
  log_address_value(my_obj2.resource_ptr);
  std::cout << my_obj + my_obj2 << std::endl;
  int a = 1;
  int b = 2; 
  char c = '2';
  is_even(1);
  is_even(c);
  std::cout << func(a, b) << std::endl;
}

