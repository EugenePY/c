#include <iostream>
#include "obj-cycle.h"

MyObject::MyObject() {
     std::cout << "create a object" << std::endl;
};

MyObject::~MyObject() 
{
      std::cout << "delete a object" << std::endl;
};

template <typename T>
void log_address_value(T var) {
  std::cout << "Address of variable " << &var << std::endl;
  std::cout << "value of variable " << var << std::endl;
  std::cout << "dereference of variable " << *var << std::endl;

}

void life_cycle_main(){
  MyObject obj;
};
