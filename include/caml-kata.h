#ifndef CAML_KATA_H
#define CAML_KATA_H

#include <iostream>

int my_func(int a);

long queueTime(std::vector<int> customers, int n);

class ProblemSet
{
 public:
  virtual void check() = 0;
  virtual void setup() = 0;

};

class QueueTime: public ProblemSet
{
  public:
    void check();
    void setup();

};

// template should put in the header file.
template <typename T, size_t SIZE>
class array {
  T _data[SIZE]; 
  static_assert(std::is_nothrow_constructible<T>::value, 
                "T must be notthrow constuctible");
  public:
   array() {};
   int size() {return SIZE;};
};



#endif
