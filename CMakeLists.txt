cmake_minimum_required(VERSION 3.10)

# Project Name
project(CKata VERSION 0.1)
set(PROJECT_SRC_DIR src)
set(PROJECT_INCLUDE_DIR include)

# SRC files
file(GLOB SRC_FILES ${PROJECT_SRC_DIR}/*.cpp)

# set the project name and version
execute_process(
  COMMAND git log -1 --format=%h
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  OUTPUT_VARIABLE GIT_COMMIT_HASH
  OUTPUT_STRIP_TRAILING_WHITESPACE
  )

# adding executables
add_executable(CKata main.cpp)
include_directories(${PROJECT_INCLUDE_DIR})

# add configs
configure_file(projectConfig.h.in projectConfig.h)
target_include_directories(CKata PUBLIC ${PROJECT_BINARY_DIR})

# CXX
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# adding Libs

add_library(caml-kata ${PROJECT_SRC_DIR}/caml-kata.cpp)
add_library(obj-cycle ${PROJECT_SRC_DIR}/obj-cycle.cpp)
add_library(pointers ${PROJECT_SRC_DIR}/pointers.cpp)

target_link_libraries(CKata PUBLIC caml-kata)

# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
target_include_directories(CKata PUBLIC
                          ${PROJECT_BINARY_DIR}
                          ${PROJECT_SOURCE_DIR}
                          )
# Testing
enable_testing()
add_subdirectory(tests)

