#include <iostream>
#include <projectConfig.h>
#include <caml-kata.h>

int main(int arg, char *argv[]){
  // report version
  std::cout << "CKata"
            << " Version " << CKata_VERSION_MAJOR << "."
            << CKata_VERSION_MINOR << std::endl;
  std::cout << "Usage: "
            << "CKata"
            << " number" << std::endl;
  return 1;
}
