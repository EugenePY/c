# C++ template

```
template <class type> ret-type func-name(parameter list) {
   // body of function
} 
```

```c++
template <typename T>
T minimum(const T& lhs, const T& rhs)
{
    return lhs < rhs ? lhs : rhs;
}
```
