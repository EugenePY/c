# Pointer
1. Pointer is the memory address that stores the value.

Basic Declaration
```c++
int *ptr;
*ptr  \\ get value that stored at the point 
ptr \\ read the value of the pointer
&ptr \\ address of the point
```

dereference
```
*ptr = 10;
```

# Function Pointer

```c++
int func(int a, int b) {
	return a * b
}

void main(){
	int (*func_ptr)(int, int) = &func;
}
```

## Memory Management
We define a object class that redefine the operator +
what we want to achieve is to define a + (b + c)
```
class Object {
 public:
  int *resource_ptr;
  Object(int a) {
    resource_ptr = new int(a);
    std::cout << resource_ptr << std::endl;
  };

  ~Object(){
    if (resource_ptr != NULL) {
      delete resource_ptr;
    }
  };

  int operator+ (Object b) {
    return *b.resource_ptr + *resource_ptr;
  };
  int *value() {return resource_ptr;}
};
```
The below code will get an error 

```
  Object my_obj(1);
  Object my_obj2(2);
  log_address_value(my_obj.value());
  log_address_value(my_obj2.value());
  std::cout << my_obj + my_obj2 << std::endl;
  std::cout << "obj2-value=" << *my_obj2.value() << std::endl;
```
the reason when the above code will raise it's because the operator+ pass object as variable since the every { } will delete all the variables in the scope so the object b is deleted.
