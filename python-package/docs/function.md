# Function

1. C++ Function
declaration
```
<attr-spec-seq(optional)> <specifiers-and-qualifiers> <parameter-list-declarator> <function-body>
```

```c++
int main(int &a, long &b) 
{
	return 0;
}
```
