# Decision Tree Algorithm

## CART Alogrithm

### Sketch the algorithm

given a dataset with 

$$
(X, Y) \\ 
X \in R^{L} \\ 
Y \in {0, 1} \\
$$
we iterate the $X[k]$

### Loss Function

1. cross entropy(same with likelihood when there no hidden variable)
the y follow a ber(p=P) distribution

$$
P(y=1) = f(x) \\
J(P) = P^{y}(1 - P)^{1-y} \\
yln(f(x)) + (1-y)ln(1 - f(x)) \\
$$

### data strucute

binary tree

```
#include <iostream>

class TreeInput {

}

class TreeTarget{

}

class TreeDataPair {
		TreeTarget target;
		TreeInput input;

	public:
		TreeDataPair(){};
		~TreeDataPair();

		// getter
		TreeTarget get_target() {return target;}
		TreeTarget get_target() {return target;}

		}

class BinaryTreeTarget: public TreeTarget{

}

class Tree {
public:
	Node root_node();
	int predict(TreeInput x);
}

class Node 
{
	bool is_root;
public: 
	Node();
	~Node();
	void inference(TreeDataPair XY);
	Node left_child();
	Node right_child();
	Node parent();
}

class CartNode: public Node {
}

template <typename INPUT_T, typename TARGET_T>
class BinaryClassifier {
	public: 
		virtual void fit(INPUT_T x, TARGET_T, y);

}


class CartTree: public BinaryClassifier<TreeInput, TreeTarget>{
	public:
		float loss();

		std::tuple<vector<int>, vector<int>> split(
						   TreeDataPair &data, int col_idx, 
						   float split_threshold) {

			std::vector left_child, std::vector right_child;
			x_col = data.X.get_col(col_idx);
			sort(&x_col);
			return find_mid_split(x_col, split_threshold, 0, x_col.lenght());
		}

		float inpurity(vector<int> row_idx, TreeTarget target)
		{
			vector<float> selected_y = target.get(row_idx);

		}
		
}
```



### recursive function
