from skbuild import setup # type: ignore

if __name__ == "__main__":
    setup(setup_requires=['setuptools_scm', "setuptools", "wheel",
                          "scikit-build", "cmake", "ninja"],
          use_scm_version=True)
