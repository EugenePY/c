#include "gtest/gtest.h"
#include "caml-kata.h"
#include <iostream>

TEST(blaTest, test1) {
  EXPECT_EQ(my_func(0),  0);
}

TEST(queueTime, test1) {
  EXPECT_EQ(queueTime(std::vector<int>{}, 1), 0);
  EXPECT_EQ(queueTime(std::vector<int>{1, 2, 3, 4}, 1), 10);
  EXPECT_EQ(queueTime(std::vector<int>{3, 4}, 2), 4);
  EXPECT_EQ(queueTime(std::vector<int>{2,2,3,3,4,4}, 2), 9);
  EXPECT_EQ(queueTime(std::vector<int>{1,2,3,4,5}, 100), 5);
}

TEST(Template, test1) {
  array<int, 5> my_array{};
  EXPECT_EQ(my_array.size(), 5);
}

